@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">@lang('Articles')</h1>

        @foreach($articles as $article)
            {{ $article }}
        @endforeach
    </div>
@endsection

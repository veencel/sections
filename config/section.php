<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sections
    |--------------------------------------------------------------------------
    |
    | This options helps to identify the section from the translated route parameter.
    |
    */
    'names' => [
        'one',
        'two',
        'three'
    ]
];

<?php
/**
 * @author    Vencel Katai <vencel.katai@webapix.hu>
 * @copyright WEBAPIX Kft. 2018 All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

namespace App\Scopes;

use App\Contracts\SectionType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class FiltersSectionType implements Scope
{
    /**
     * @var SectionType
     */
    private $section;

    public function __construct(SectionType $section)
    {
        $this->section = $section;
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model   $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('section_type', $this->section->name());
    }
}

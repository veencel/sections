<?php
/**
 * @author    Vencel Katai <vencel.katai@webapix.hu>
 * @copyright WEBAPIX Kft. 2018 All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

namespace App\Http\Controllers;

use App\Services\SectionManager;

class SectionController extends Controller
{

    /**
     * Execute an action on the controller.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        $sectionType = $parameters['section_type'];

        app(SectionManager::class)->setupSection($sectionType);

        unset($parameters['section_type']);

        return parent::callAction($method, $parameters);
    }
}

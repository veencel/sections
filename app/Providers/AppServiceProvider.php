<?php

namespace App\Providers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerBootstrapMacro();
    }

    public function registerBootstrapMacro()
    {
        Blueprint::macro('section', function () {
            $this->enum('section_type', ['one', 'two', 'three']);
        });
    }
}

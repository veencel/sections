<?php
/**
 * @author    Vencel Katai <vencel.katai@webapix.hu>
 * @copyright WEBAPIX Kft. 2018 All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

namespace App\Services;

use App\Contracts\SectionType;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class SectionManager
{
    /**
     * @var array
     */
    private $sections;

    public function __construct()
    {
        $this->sections = config('section.names');
    }

    public function setupSection($section)
    {
        $section = $this->resolveSection($section);

        $this->addGlobalScopes($section);
        $this->addViewComposer($section);
    }

    /**
     * @param $section
     * @return SectionType
     */
    private function resolveSection($section)
    {
        $name = $this->findSectionName($section);

        if (! $name) {
            abort(404);
        }

        return app('App\Sections\\' . Str::studly($name));
    }

    private function findSectionName($section)
    {
        return $this->translatedSectionNames()[$section] ?? null;
    }

    private function addGlobalScopes(SectionType $section)
    {
        foreach ($section->modelScopes() as $model => $scopes) {
            foreach ($scopes as $scope) {
                $model::addGlobalScope(is_object($scope) ? $scope : app($scope));
            }
        }
    }

    private function addViewComposer(SectionType $section)
    {
        View::composer('layouts.app', function ($view) use ($section) {
            $view->with('sectionClass', $section->bodyClass());
        });
    }

    private function translatedSectionNames()
    {
        $locale = config('app.locale');

        return collect($this->sections)->mapWithKeys(function ($section) use ($locale) {
            return [__($section, [], $locale) => $section];
        });
    }
}

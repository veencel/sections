<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Article extends Model
{
    use HasTranslations;

    public $translatable = [
        'name', 'description'
    ];

    public function __toString()
    {
        return $this->name . ' ' . $this->section_type;
    }
}

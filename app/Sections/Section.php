<?php
/**
 * @author    Vencel Katai <vencel.katai@webapix.hu>
 * @copyright WEBAPIX Kft. 2018 All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

namespace App\Sections;

use App\Article;
use App\Contracts\SectionType;
use App\Scopes\FiltersSectionType;
use Illuminate\Support\Str;

abstract class Section implements SectionType
{
    public function modelScopes(): array
    {
        return [
            Article::class => [new FiltersSectionType($this)],
        ];
    }

    public function name(): string
    {
        return Str::kebab(class_basename($this));
    }
}

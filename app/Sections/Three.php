<?php
/**
 * @author    Vencel Katai <vencel.katai@webapix.hu>
 * @copyright WEBAPIX Kft. 2018 All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

namespace App\Sections;

class Three extends Section
{
    public function bodyClass(): string
    {
        return 'three';
    }
}
